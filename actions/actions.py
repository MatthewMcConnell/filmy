# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

import os

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, FollowupAction

import tmdbsimple as tmdb
tmdb.API_KEY = os.environ.get("TMDB_KEY")


# load list of genres
genre_client = tmdb.Genres()
movie_genre_list = genre_client.movie_list()["genres"]
tv_genre_list = genre_client.tv_list()["genres"]
# convert list of genres to dictionary of form {genre_name: tmdb_genre_id}
movie_genre_lookup = {genre["name"]: str(
    genre["id"]) for genre in movie_genre_list}
tv_genre_lookup = {genre["name"]: str(
    genre["id"]) for genre in tv_genre_list}


# loading configuration for getting base url for images
BASE_URL = tmdb.Configuration().info(
)["images"]["secure_base_url"] + "original"


class FindMediaRecommendation(Action):

    def name(self) -> Text:
        return "action_recommend_media"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        if tracker.get_slot("wants_tv"):
            events = [FollowupAction("action_recommend_tv")]
        else:
            events = [FollowupAction("action_recommend_movie")]

        return events


class FindMovieRecommendation(Action):

    def name(self) -> Text:
        return "action_recommend_movie"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        discover_client = tmdb.Discover()
        full_info_movies = discover_client.movie(
            with_genres=get_genre_id(tracker.get_slot("genre")),
            with_cast=get_person_id(tracker.get_slot("starring")),
            with_crew=get_person_id(tracker.get_slot("director")))["results"]

        if len(full_info_movies) == 0:
            full_info_movies = discover_client.movie()["results"]
            dispatcher.utter_message(
                text="I couldn't find a movie that matched your requirements, but...")

        movies = [{"id": movie["id"], "title": movie["title"],
                   "aggregate_rating": movie["vote_average"]} for movie in full_info_movies]

        # remove first result as recommendation
        movie_raw = movies.pop(0)
        movie = get_media_info(movie_raw)

        # Utter the recommendation
        dispatcher.utter_message(
            template="utter_recommend",
            title=movie.get("title", None),
            genre=movie.get("genre", None),
            aggregate_rating=movie.get("aggregate_rating", None),
            director=movie.get("director", None),
            starring=movie.get("starring", None)
        )

        events = list()

        # Remember current_recommendation
        events.append(SlotSet("current_recommendation", movie))

        # Set latest list of recommendations that could be recommended in the future
        events.append(SlotSet("recommendations", movies))

        # reset query slots
        events.append(SlotSet("director"))
        events.append(SlotSet("starring"))
        events.append(SlotSet("genre"))

        events.append(SlotSet("wants_tv", False))

        return events


class FindTVRecommendation(Action):

    def name(self) -> Text:
        return "action_recommend_tv"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        if tracker.get_slot("starring") != None or tracker.get_slot("director") != None:
            dispatcher.utter_message(
                text="I can't currently find TV shows with a specific actor or director, but I can still match a genre...")

        discover_client = tmdb.Discover()
        full_info_tv = discover_client.tv(
            with_genres=get_genre_id(tracker.get_slot("genre"), True))["results"]

        if len(full_info_tv) == 0:
            full_info_tv = discover_client.tv()["results"]
            dispatcher.utter_message(
                text="I couldn't find a tv show that matched your requirements, but...")

        tv_shows = [{"id": tv["id"], "title": tv["name"],
                     "aggregate_rating": tv["vote_average"]} for tv in full_info_tv]

        # remove first result as recommendation
        tv_raw = tv_shows.pop(0)
        tv = get_media_info(tv_raw, tv=True)

        # Utter the recommendation
        dispatcher.utter_message(
            template="utter_recommend_tv",
            title=tv.get("title", None),
            genre=tv.get("genre", None),
            aggregate_rating=tv.get("aggregate_rating", None),
            director=tv.get("director", None),
            starring=tv.get("starring", None)
        )

        events = list()

        # Remember current_recommendation
        events.append(SlotSet("current_recommendation", tv))

        # Set latest list of recommendations that could be recommended in the future
        events.append(SlotSet("recommendations", tv_shows))

        # reset query slots
        events.append(SlotSet("director"))
        events.append(SlotSet("starring"))
        events.append(SlotSet("genre"))

        events.append(SlotSet("wants_tv", True))

        return events


class SupplyAlternative(Action):

    def name(self) -> Text:
        return "action_supply_alternative"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        medias = tracker.get_slot("recommendations")

        events = list()

        if len(medias) > 0:
            # remove first result as recommendation
            media_raw = medias.pop(0)
            media = get_media_info(media_raw, tracker.get_slot("wants_tv"))

            if tracker.get_slot("wants_tv"):
                template = "utter_recommend_tv"
            else:
                template = "utter_recommend"

            # Utter the recommendation
            dispatcher.utter_message(
                template=template,
                title=media.get("title", None),
                genre=media.get("genre", None),
                aggregate_rating=media.get("aggregate_rating", None),
                director=media.get("director", None),
                starring=media.get("starring", None)
            )

            # Remember current_recommendation and remove it from list so to not recommend it again
            events.append(SlotSet("current_recommendation", media))

            # Set latest list of recommendations that could be recommended in the future
            events.append(SlotSet("recommendations", medias))
        else:
            dispatcher.utter_message(
                text="I ran out of recommendations so let's find some new ones...")
            events.append(FollowupAction("action_recommend_movie"))

        return events


class GiveMediaInfo(Action):

    def name(self) -> Text:
        return "action_give_media_info"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        media = tracker.get_slot("current_recommendation")

        slot_was_filled = False

        if (tracker.get_slot("genre_req")):
            slot_was_filled = True
            dispatcher.utter_message(
                template="utter_genre_info", genre=media.get("genre", "...(couldn't find the info, sorry!)"))

        if (tracker.get_slot("director_req")):
            slot_was_filled = True
            dispatcher.utter_message(
                template="utter_director_info", director=media.get("director", "...(couldn't find the info, sorry!)"))

        if (tracker.get_slot("starring_req")):
            slot_was_filled = True
            dispatcher.utter_message(
                template="utter_starring_info", starring=media.get("starring", "...(couldn't find the info, sorry!)"))

        if (not slot_was_filled):
            dispatcher.utter_message(template="utter_misunderstood")

        events = list()

        events.append(SlotSet("genre_req"))
        events.append(SlotSet("director_req"))
        events.append(SlotSet("starring_req"))

        return events


class GetImage(Action):

    def name(self) -> Text:
        return "action_get_image"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        if (tracker.get_slot("wants_tv")):
            client = tmdb.TV(tracker.get_slot("current_recommendation")["id"])
        else:
            client = tmdb.Movies(tracker.get_slot(
                "current_recommendation")["id"])

        images = client.images()
        image_link = None

        if images:
            if tracker.get_slot("image_type") and tracker.get_slot("image_type").lower() == "poster":
                image_link = BASE_URL + images["posters"][0]["file_path"]
            else:
                image_link = BASE_URL + images["backdrops"][0]["file_path"]

        if image_link:
            dispatcher.utter_message(image=image_link)
        else:
            dispatcher.utter_message(text="Sorry, I couldn't find any images")

        return []


class GetVideo(Action):

    def name(self) -> Text:
        return "action_get_video"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        if (tracker.get_slot("wants_tv")):
            client = tmdb.TV(tracker.get_slot("current_recommendation")["id"])
        else:
            client = tmdb.Movies(tracker.get_slot(
                "current_recommendation")["id"])

        videos = client.videos()["results"]
        video_dict = None

        if videos:
            if tracker.get_slot("video_type"):
                for video in videos:
                    if tracker.get_slot("video_type").lower() == video["type"].lower():
                        video_dict = video
                        break
                if video_dict == None:
                    dispatcher.utter_message(
                        text="Sorry, I couldn't find the exact type of video you were looking for but here is another video")
                    video_dict = videos[0]
            else:
                video_dict = videos[0]
        else:
            dispatcher.utter_message(
                text="Sorry, I couldn't find any videos associated with this recommendation.")

        if video_dict["site"] == "YouTube":
            src = "https://youtube.com/embed/" + video_dict["key"]
        else:
            src = "https://player.vimeo.com/video/" + video_dict["key"]

        attachment = {"type": "video", "payload": {
            "title": video_dict["name"],
            "src": src
        }}

        dispatcher.utter_message(attachment=attachment)

        return []


class GetWatchProviders(Action):

    def name(self) -> Text:
        return "action_get_providers"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        if (tracker.get_slot("wants_tv")):
            client = tmdb.TV(tracker.get_slot("current_recommendation")["id"])
        else:
            client = tmdb.Movies(tracker.get_slot(
                "current_recommendation")["id"])

        client.URLS["providers"] = "/{id}/watch/providers"
        path = client._get_id_path('providers')
        response = client._GET(path)

        providers = response["results"].get("GB", {}).get("flatrate", [])

        elements = list()

        for provider in providers:
            elements.append(
                {"title": provider["provider_name"], "image_url": BASE_URL + provider["logo_path"], "buttons": []})

        if len(elements) > 0:
            carousel = {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": elements
                }
            }

            dispatcher.utter_message(attachment=carousel)
        else:
            dispatcher.utter_message(
                text="Sorry, I couldn't find any streaming services that have the recommendation.")

        return []


def get_person_id(name):
    if not name:
        return ""

    search = tmdb.Search()
    results = search.person(query=name)["results"]

    if len(results) > 0:
        return str(results[0]["id"])
    else:
        return ""


def get_genre_id(name, tv=False):
    if not name:
        return ""

    if tv:
        return tv_genre_lookup.get(name.capitalize(), "")
    else:
        return movie_genre_lookup.get(name.capitalize(), "")


def get_media_info(media, tv=False):
    """Gets the media info for a film with tmdb id

    Args:
        id (int): tmdb movie/tv id
        title (str, optional): movie/tv show. Defaults to None.
        genre (str, optional): the genre(s) of the media. Defaults to None.
        aggregate_rating (float, optional): media's average rating. Defaults to None.
        director (str, optional): name of the director. Defaults to None.
        starring (str, optional): name of a prominent actor in the film. Defaults to None.

    Returns:
        dictionary with all info about the film
    """

    if tv:
        media_client = tmdb.TV(media["id"])
        director_str = "Producer"
    else:
        media_client = tmdb.Movies(media["id"])
        director_str = "Director"

    media_info = media_client.info()
    media_credits = media_client.credits()

    if not "title" in media:
        media["title"] = media_info["title"]

    if not "genre" in media:
        media["genre"] = "/".join([genre["name"]
                                   for genre in media_info["genres"]])

    if not "aggregate_rating" in media:
        media["aggregate_rating"] = media_info["vote_average"]

    if not "director" in media:
        for crew in media_credits["crew"]:
            if director_str in crew["job"]:
                media["director"] = crew["name"]

    if not "starring" in media:
        if media_credits["cast"]:
            media["starring"] = media_credits["cast"][0]["name"]

    return media
