# Filmy

This is a Film and TV recommendations bot created in Rasa and Python for coursework during my time at the University of Glasgow. Currently designed with work with the [Rasa Web Chat](https://github.com/botfront/rasa-webchat) widget. 

![Example of Filmy Interaction](https://gitlab.com/MatthewMcConnell/filmy/-/raw/master/misc-doc/example.png)

Ideally I would convert this to be containerised and set this up on a dynamic server. However, you can see how the agent is designed and works locally in a short [presentation video](https://youtu.be/3CsM6hgzXwY).